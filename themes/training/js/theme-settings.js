(function ($, Drupal) {
  /*
   * Page: My Requests
   */

  Drupal.behaviors.pageMyRequestsAndPersonsEntitled = {
    attach: function (context) {
      if ($('#table-list-visits').length > 0) {
      	$('#table-list-visits .table-responsive a').once('container-preview').each(function () {
        
        });
      }
    }
  };

  Drupal.behaviors.visit_rate = {
    attach: function (context) {
      if ($('#rateYo').length > 0) {
        $("#rateYo").rateYo({
          normalFill: "#303030",
          ratedFill: "#FFFFFF"
        });
      }

      if ($('.rateYoLists').length > 0) {
        $('.rateYoLists').each(function(index) {
          $( this ).rateYo({
            normalFill: "#8f5910",
            ratedFill: "#eebb2b",
            "rating": parseInt($( this ).text())
          });
        });
      }
    }
  };

  Drupal.behaviors.visit_rate_update = {
    attach: function (context) {
      if ($('#block-clientproviderrate').length > 0) {

        // Close rating block.
        $('#block-clientproviderrate a#close-rate').click(function(){
          var id = $(this).attr('class');
          var rating = 0;
          
          $.ajax({
            url: '/visit/rate/' + id + '/' + rating, 
            data: {}, 
            success : function(data) {
              location.reload();
            }
          });

          return false;
        });

        // Set rating value.
        $("#rateYo").click(function () {
            // Get rating.
            var rating = parseInt($("#rateYo").rateYo("rating")) + 1;
            var id = parseInt($(this).attr('class'));
            
            $.ajax({
              url: '/visit/rate/' + id + '/' + rating, 
              data: {}, 
              success : function(data) {
                location.reload();
              }
            });
        });

      }
    }
  };


})(jQuery, Drupal);