<?php

namespace Drupal\hello_world\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Hello World Links block.
 *
 * @Block(
 *  id = "hello_world_links_block",
 *  admin_label = @Translation("Hello world Links"),
 * )
 */
class HelloWorldLinksBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    return [      
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#title' => $this->t('Links A'),
      '#items' => [
        [
          '#markup' => Link::fromTextAndUrl(t('Add Article'), Url::fromRoute('node.add', ['node_type' => 'article']))->toString(),
          '#wrapper_attributes' => ['class' => 'add_article']
        ],
        [
          '#markup' => Link::fromTextAndUrl(t('Add Content'), Url::fromUri('internal:/admin/content', ['attributes' => ['target' => '_blank', 'class' => 'aaaa'], 'query' => ['d' => 'q']]))->toString(),
          '#wrapper_attributes' => ['class' => 'add_content'],
        ],
        [
          '#markup' => Link::fromTextAndUrl(t('Hello'), Url::fromRoute('hello_world.hello'))->toString(),
          '#wrapper_attributes' => ['class' => 'hello']
        ],
      ],
    ];
  }

}
