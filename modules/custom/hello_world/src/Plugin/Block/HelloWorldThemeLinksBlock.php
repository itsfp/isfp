<?php

namespace Drupal\hello_world\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Hello World Links Theme block.
 *
 * @Block(
 *  id = "hello_world_links_theme_block",
 *  admin_label = @Translation("Hello world Theme Links"),
 * )
 */
class HelloWorldThemeLinksBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    return [      
      '#theme' => 'custom_item_list',
      '#list_type' => 'ul',
      '#title' => $this->t('Custom Links 2'),
      '#items' => [
        [
          'link' => Link::fromTextAndUrl(t('Add Article'), Url::fromRoute('node.add', ['node_type' => 'article']))->toString(),
          'class' => 'add_article'
        ],
        [
          'link' => Link::fromTextAndUrl(t('Add Content'), Url::fromUri('internal:/admin/content'))->toString(),
          'class' => 'add_content'
        ],
        [
          'link' => Link::fromTextAndUrl(t('Hello'), Url::fromRoute('hello_world.hello'))->toString(),
          'class' => 'hello'
        ],
      ],
    ];
  }

}
