<?php
namespace Drupal\hello_world\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\hello_world\HelloWorldSalutation;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountProxy;

/**
 * Controller for the salutation message.
 */
class HelloWorldController extends ControllerBase {

  /**
   * @var \Drupal\hello_world\HelloWorldSalutation
   */
  protected $salutation;

  /**
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $current_user;

  /**
   * HelloWorldController constructor.
   *
   * @param \Drupal\hello_world\HelloWorldSalutation $salutation
   */
  public function __construct(HelloWorldSalutation $salutation, AccountProxy $current_user) {
    $this->salutation = $salutation;
    $this->current_user = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('hello_world.salutation'),
      $container->get('current_user')
    );
  }

  /**
   * Hello World.
   *
   * @return string
   */
  public function helloWorld() {
    return [
      '#markup' => $this->current_user->getEmail() . ', ' . $this->salutation->getSalutation(),
    ];
  }

}
