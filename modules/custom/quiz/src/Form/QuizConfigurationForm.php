<?php

namespace Drupal\quiz\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form definition for the Quiz page.
 */
class QuizConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['quiz.answers'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'quiz_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('quiz.answers');

    $form['question'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Add the question:'),
      '#description' => $this->t('Please add the question to do.'),
      '#default_value' => $config->get('question'),
    ];

    $form['correct_answer'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Correct Answer:'),
      '#description' => $this->t('Please provide the correct answer to the question.'),
      '#default_value' => $config->get('answer'),
    ];

    $form['wrong_answers'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Wrong Answer:'),
      '#description' => $this->t('Please provide the wrong answers separate by comma to use.'),
      '#default_value' => $config->get('wrong_answer'),
    ];

    return parent::buildForm($form, $form_state);
  }

    /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // Validate if question is empty.
    if (empty($form_state->getValue('question'))) {
      $form_state->setErrorByName('question', $this->t("Please add a question."));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Storing Question.
    $this->config('quiz.answers')
         ->set('question', $form_state->getValue('question'))
         ->save();

    // Storing Correct Answer.     
    $this->config('quiz.answers')
         ->set('answer', $form_state->getValue('correct_answer'))
         ->save();

    // Storing Wrong Answers.    
    $this->config('quiz.answers')
         ->set('wrong_answer', $form_state->getValue('wrong_answers'))
         ->save();

    parent::submitForm($form, $form_state);
  }

}
