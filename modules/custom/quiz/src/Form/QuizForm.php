<?php

namespace Drupal\quiz\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * form for the Quiz page.
 */
class QuizForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['quiz.answers'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'quiz_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('quiz.answers');

    $form['custom_title'] = [
      '#markup' => '<h2>' . $this->t('Answer the following questions') . '</h2>',
    ];

    $form['question'] = array(
      '#markup' => '<h2>' . $config->get('question') . '=</h2>',
    );

    // Get Options and set them as random.
    $all_values = $this->getQuizValues();
    $answer = $config->get('answer');
    $keys = array_keys($all_values); 
    shuffle($keys); 
    $random = array(); 
    foreach ($keys as $key) { 
      $random[$key] = $all_values[$key]; 
    }

    // Show options.
    $form['choose'] = [
      '#type' => 'radios',
      '#options' => $random,
    ];

    $form['options'] = [
      '#type' => 'hidden',
      '#value' => $answer,
    ];

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
      '#attributes' => array('class' => array('custom-submit')),
    );

    return $form;

    //return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('quiz.answers');

    // Validate if answer is not empty.
    if ($form_state->getValue('choose') == NULL) {
      $form_state->setErrorByName('choose', $this->t("Please choose an option."));
    }
    else {
      $all_values = $this->getQuizValues();
      $options = explode("," , $form_state->getValue('options'));
      if ($all_values[$form_state->getValue('choose')] != $config->get('answer')) {
        $form_state->setErrorByName('choose', $this->t("Really bro?."));
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    drupal_set_message($this->t('You\'re the best! :-)'), 'status');

    parent::submitForm($form, $form_state);
  }


  /**
   * Method to get values to choose.
   */
  public function getQuizValues() {
    $config = $this->config('quiz.answers');

    // Get correct answer.
    $answer = $config->get('answer');

    // Get wrong answers and convert it into an array.
    $wrong_answers =  explode(",", $config->get('wrong_answer'));

    // Merge correct ansnwer into array with wrong answer.
    return array_merge($wrong_answers, [$answer]);
  }

}

