<?php

namespace Drupal\storing\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form definition for the Storing page.
 */
class StoringConfigurationForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['storing.configuration'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'storing_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // State.
    $form['state'] = [
      '#type' => 'textfield',
      '#title' => $this->t('State:'),
      '#description' => $this->t('Test storing state.'),
      '#default_value' => \Drupal::state()->get('storing_state', 'Nothing'),
    ];

    $form['remove_state'] = array(
      '#type' => 'checkbox',
      '#title' => t('Remove State'),
    );

    // Set Multiple.
    $values = \Drupal::state()->getMultiple(['state_multiple_1', 'state_multiple_2']);
    $form['state_multiple_1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('State Multiple 1:'),
      '#description' => $this->t('Test storing state.'),
      '#default_value' => (isset($values['state_multiple_1']) ? $values['state_multiple_1'] : 'Nothing'),
    ];

    $form['state_multiple_2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('State Multiple 2:'),
      '#description' => $this->t('Test storing state.'),
      '#default_value' => (isset($values['state_multiple_2']) ? $values['state_multiple_2'] : 'Nothing'),
    ];

    $form['remove_multiple'] = array(
      '#type' => 'checkbox',
      '#title' => t('Remove Multiple State'),
    );

    return parent::buildForm($form, $form_state);
  }

    /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Storing state.
    if ($form_state->getValue('remove_state')) {
      \Drupal::state()->delete('storing_state');
    }
    else {
      \Drupal::state()->set('storing_state', $form_state->getValue('state'));
    }
    // Storing state multiple.
    if ($form_state->getValue('remove_multiple')) {
      \Drupal::state()->deleteMultiple([
        'state_multiple_1',
        'state_multiple_2']);
    }
    else {
      \Drupal::state()->setMultiple([
        'state_multiple_1' => $form_state->getValue('state_multiple_1'), 
        'state_multiple_2' => $form_state->getValue('state_multiple_2')]);
    }

    drupal_set_message("Esto es un Mensaje");
    drupal_set_message("Esto es un Error", 'error');
    drupal_set_message("Esto es un Warning", 'warning');

    parent::submitForm($form, $form_state);
  }

}
